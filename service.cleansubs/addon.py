# ============================================================
# KODI Clean Subs - Version 6.4 by D. Lanik (2016)
# ------------------------------------------------------------
# Clean up downloaded subs from ads, etc
# ------------------------------------------------------------
# License: GPL (http://www.gnu.org/licenses/gpl-3.0.html)
# ============================================================

import xbmc
import xbmcaddon
import os.path
import sqlite3
import mysql.connector
import base64
import time

# ============================================================
# Sync UUID -> remote database
# ============================================================


def SyncUUID():
    global __addon__
    global strHost2
    global strUser2
    global strPassword2
    global strDatabase2

    machineID = __addon__.getSetting('ID')

    try:
        myconn = mysql.connector.connect(host=strHost2, user=strUser2, password=strPassword2, database=strDatabase2, charset="utf8", use_unicode=True, buffered=True)
        myc = myconn.cursor()

        myc.execute('SET NAMES utf8;')
        myc.execute('SET CHARACTER SET utf8;')
        myc.execute('SET character_set_connection=utf8;')

        sqlstr = "SELECT uuid FROM uuid WHERE uuid='" + machineID + "'"

        myc.execute(sqlstr)
        data = myc.fetchone()

        if data is None:
            xbmc.log("CLEANSUBS >> UUID NOT IN DATABASE, WILL ADD")
            sqlstr = "INSERT INTO uuid(uuid) values ('" + machineID + "')"
            try:
                myc.execute(sqlstr)
                myconn.commit()
                __addon__.setSetting('IDSynced', "1")
            except Exception as e:
                xbmc.log("CLEANSUBS >> SQL ERROR IN SyncUUID2: %s" % str(e))
        else:
            xbmc.log("CLEANSUBS >> UUID ALREADY IN DATABASE")

        myconn.close()
    except Exception as e:
        xbmc.log("CLEANSUBS >> SQL ERROR IN SyncUUID1: %s" % str(e))

# ============================================================
# Sync Stats local -> remote database
# ============================================================


def SyncStats():
    global __addondir__
    global strHost2
    global strUser2
    global strPassword2
    global strDatabase2

    sqlite_file = os.path.join(__addondir__, 'cleansubs.sqlite')
    conn = sqlite3.connect(sqlite_file)
    c = conn.cursor()
    err = 0

    try:                           # try uploading stats to server
        start = time.time()
        myconn = mysql.connector.connect(host=strHost2, user=strUser2, password=strPassword2, database=strDatabase2, charset="utf8", use_unicode=True)
        myc = myconn.cursor()

        myc.execute('SET NAMES utf8;')
        myc.execute('SET CHARACTER SET utf8;')
        myc.execute('SET character_set_connection=utf8;')

        try:
            myc.execute("SELECT COUNT(*) FROM stats")
            numberOfColumns = int(myc.fetchone()[0])
            xbmc.log("CLEANSUBS >> NUMBER OF RECORDS IN MYSQL (PRIOR): %s" % (str(numberOfColumns)))
        except Exception as e:
            xbmc.log("CLEANSUBS >> SQL ERROR IN SyncStatsB: %s" % str(e))

        myconn.close()
        myconn = mysql.connector.connect(host=strHost2, user=strUser2, password=strPassword2, database=strDatabase2, charset="utf8", use_unicode=True, buffered=True)
        myc = myconn.cursor()

        c.execute('SELECT * FROM stats')
        i = 0
        for row in c:
            strParams = (row[2], row[3])
            myc.execute("SELECT pattern FROM stats WHERE md5 = %s AND row = %s", (strParams))

            data = myc.fetchone()

            if data is None:
                if row[5] == 'heuristics_suspicious':
                    strParams = (row[6],)
                    myc.execute("SELECT text FROM newdefs WHERE text = %s", strParams)
                    data2 = myc.fetchone()

                    if data2 is None:
                        i += 1
                        myc.execute("INSERT INTO stats(date, uuid, md5, row, format, pattern, text) values (%s, %s, %s, %s, %s, %s, %s)", row)
                        myconn.commit()
                else:
                    i += 1
                    myc.execute("INSERT INTO stats(date, uuid, md5, row, format, pattern, text) values (%s, %s, %s, %s, %s, %s, %s)", row)
                    myconn.commit()

        xbmc.log("CLEANSUBS >> ADDED ROWS TO MYSQL: %s" % (str(i)))

        myconn.close()
        myconn = mysql.connector.connect(host=strHost2, user=strUser2, password=strPassword2, database=strDatabase2, charset="utf8", use_unicode=True)
        myc = myconn.cursor()

        try:
            myc.execute("SELECT COUNT(*) FROM stats")
            numberOfColumns = int(myc.fetchone()[0])
            xbmc.log("CLEANSUBS >> NUMBER OF RECORDS IN MYSQL (AFTER): %s" % (str(numberOfColumns)))
        except Exception as e:
            xbmc.log("CLEANSUBS >> SQL ERROR IN SyncStatsA: %s" % str(e))

        myconn.close()
        end = time.time()
        xbmc.log('CLEANSUBS >> CLOSE SQL REMOTE1 IN %0.2f SECONDS' % (end - start))
        err = 0
    except Exception as e:
        xbmc.log("CLEANSUBS >> SQL ERROR IN SyncStats1: %s" % str(e))
        err = 1

    if err == 0:                # if upload succeded, delete stats from local database
        try:
            c.execute("DELETE FROM stats")
            conn.commit()
            xbmc.log("CLEANSUBS >> EMPTIED LOCAL DB")
            conn.close()
            xbmc.log("CLEANSUBS >> CLOSE SQL LOCAL")
        except Exception as e:
            xbmc.log("CLEANSUBS >> SQL ERROR IN SyncStats")

    return

# ============================================================
# Shift string
# ============================================================


def strShift(line, value):
    nice_str_chars = ""
    line = line.strip()
    for char in line:
        int_of_char = ord(char)
        int_of_char += value
        nice_char = chr(int_of_char)
        nice_str_chars += nice_char
    return nice_str_chars

# ============================================================
# ------------------------------------------------------------
# Main
# ------------------------------------------------------------
# ============================================================


__addon__ = xbmcaddon.Addon(id='service.cleansubs')
__addonwd__ = xbmc.translatePath(__addon__.getAddonInfo('path').decode("utf-8"))
__addondir__ = xbmc.translatePath(__addon__.getAddonInfo('profile').decode('utf-8'))
__version__ = __addon__.getAddonInfo('version')

strHost2 = base64.b64decode(strShift("`C5p^Tgq]CRo]S5k^/J,Hi5uVs99", 4))
strUser2 = base64.b64decode(strShift("U.thUS1v`SFb].5g]M99", 4))
strPassword2 = base64.b64decode(strShift("KPVJKCF]PDdd`s99", 4))
strDatabase2 = base64.b64decode(strShift("U.thUS1v`SFb_/Nd`DI9", 4))

# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------
